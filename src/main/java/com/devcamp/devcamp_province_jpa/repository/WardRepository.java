package com.devcamp.devcamp_province_jpa.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.devcamp_province_jpa.models.Ward;

public interface WardRepository extends JpaRepository<Ward, Long> {
    ArrayList<Ward> findByDistrictId(int id);
}
