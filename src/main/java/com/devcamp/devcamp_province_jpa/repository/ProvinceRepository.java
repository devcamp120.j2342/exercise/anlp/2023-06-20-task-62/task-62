package com.devcamp.devcamp_province_jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.devcamp_province_jpa.models.Province;

public interface ProvinceRepository extends JpaRepository<Province, Long>{
    
}
