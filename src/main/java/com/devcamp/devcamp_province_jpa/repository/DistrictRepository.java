package com.devcamp.devcamp_province_jpa.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.devcamp_province_jpa.models.District;

public interface DistrictRepository extends JpaRepository<District, Long> {
    ArrayList<District> findByProvinceId(int id);
}
