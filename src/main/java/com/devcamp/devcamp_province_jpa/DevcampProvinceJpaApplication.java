package com.devcamp.devcamp_province_jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevcampProvinceJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevcampProvinceJpaApplication.class, args);
	}

}
